package bcas.ap.insert.array;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Demo {
	public static void main(String[] args) {

		while (true) {
			Scanner scan = new Scanner(System.in);

			System.out.println("\n\nEnter The Number:");
			try {

				int num = scan.nextInt();
				System.out.println("Enter The Number Range:");
				int rangen = scan.nextInt();
				ArrayTest.createRanArray(num, rangen);

			} catch (InputMismatchException e) {
				System.out.println("Please Enter Only Numbers");
				System.out.println("And Please Enter It In Your Given Range");
			}

		}

	}
}
